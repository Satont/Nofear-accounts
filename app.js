var express = require('express')
    app = express()
    server = app.listen(process.env.PORT || 3000)
    io = require('socket.io').listen(server)
    config = require('./config')
    TeamspeakQuery = require('teamspeak-query')
    query = new TeamspeakQuery(config.ts.host, 10011)
    _ = require('lodash')

app.enable('trust proxy')
app.use(express.static('public'))
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('X-powered-by', 'Nofear Accounts by Satont')
  next()
})

query.send('login', config.ts.login, config.ts.pass)
  .then(() => query.send('use', 1))
  .then(() => console.log('Connected to ts'))
  .then(() => query.send('clientupdate', {'client_nickname': config.ts.nick}))
  .catch(err => console.error('Not connected ts:', err))

query.keepalive.enable(true)
query.keepalive.duration = 30000

function parseList(data) {
    return data.raw()
      .split('|')
      .map(TeamspeakQuery.parse)
      .map(entry => entry.params);
  }

io.on('connection', function(client){
    var ip 
    client.handshake.headers["x-real-ip"] !== undefined ? ip =  client.handshake.headers['x-real-ip'] : ip = client.handshake.headers['X-Forwarded-For']
    console.log(ip)
    var user
    var FLAG = false
    query.send('clientlist', '-ip -groups -info')
      .then(parseList)
      .then(list => list.find(client => client.client_type == 0 && client.connection_client_ip == ip))
      .then(result =>{
        if (result) {
          var userGroups = result.client_servergroups.split(",").map(Number)
          userGroups.includes(68) ? client.emit('auth', {nick: result.client_nickname, verifyed: true}) : client.emit('auth', {nick: result.client_nickname, verifyed: false})
        } else client.emit('notauth')
      })

      client.on('verify', function(){
        query.send('clientlist', '-ip -groups -info')
        .then(parseList)
        .then(list => list.find(client => client.client_type == 0 && client.connection_client_ip == ip))
        .then(result => {
          var userGroups = result.client_servergroups.split(",").map(Number)
          if (userGroups.includes(68)) return
          query.send('servergroupaddclient', {'sgid': 68, 'cldbid': result.client_database_id})
          client.emit('auth', {nick: result.client_nickname, verifyed: true})
        })
        })
})

